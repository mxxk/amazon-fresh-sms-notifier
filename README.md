# Amazon Fresh SMS Notifier

## Setup

1. Check out this repository.

1. Create account on Heroku and add credit card to verify account for total of [1000 free dyno hours per month](https://www.heroku.com/free).

1. [Install Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli).

1. Create a new Heroku app by running

    ```sh
    heroku create REPLACE-WITH-APP-NAME
    ```

    from within the repository root.

## Configuration

This section sets up config variables for the Heroku app. This can be done using (1) the web interface (https://dashboard.heroku.com/apps/REPLACE-WITH-APP-NAME/settings) or (2) the Heroku CLI:

```sh
heroku config:set -a REPLACE-WITH-APP-NAME 'AMAZON_URL=https://www.amazon.com/alm/storefront?almBrandId=QW1hem9uIEZyZXNo&ref_=nav_cs_fresh'
```

1. Amazon

    1. Set the Heroku app config variable `AMAZON_URL` to `https://www.amazon.com/alm/storefront?almBrandId=QW1hem9uIEZyZXNo&ref_=nav_cs_fresh` (or whatever URL you see in the browser when you navigate to the storefront of Amazon Fresh).

    1. Make a request to Amazon in your favorite browser where you can inspect the request data in the browser's developer tools.

    1. Steal the contents of the `Cookie` and `User-Agent` headers.

    1. Format a JSON object with these two headers. For example:

        ```js
        JSON.stringify({ Cookie: '...', 'User-Agent': '...' });
        ```

    1. Set the Heroku app config variable `AMAZON_HEADERS` to this JSON string.

1. Twilio (for SMS notifications)

    1. [Create account on Twilio](https://www.twilio.com/try-twilio) and create a project for sending SMS messages.

    1. Set the Heroku app config variable `TWILIO_HEADERS` to `https://api.twilio.com/2010-04-01/Accounts/REPLACE-WITH-TWILIO-ACCOUNT/Messages.json`.

    1. Figure out how to make Twilio API requests, and copy the Basic Authentication header into a JSON object:

        ```js
        JSON.stringify({ Authorization: 'Basic ...' });
        ```

    1. Set the Heroku app config variable `TWILIO_HEADERS` to this JSON string.

    1. Choose a Twilio phone number.

    1. Format a JSON object with the contents of the Twilio API request, replacing the `From` field with your Twilio phone number and the `To` field with the number on which you want to receive texts:

        ```js
        JSON.stringify({
            From: '+12223334444',
            To: '+12223334444',
            Body: 'Amazon Fresh delivery window available',
        })
        ```

    1. Set the Heroku app config variable `TWILIO_PAYLOAD` to this JSON string.

1. Heroku API token

    1. Create a new Heroku API token:

        ```sh
        heroku authorizations:create --short --scope=write-protected
        ```

    1. Set the Heroku app config variable `HEROKU_API_TOKEN` to the string output above.

    1. Set the Heroku app config variable `HEROKU_APP_ID` to the name of the Heroku app (`REPLACE-WITH-APP-NAME` as referenced earlier).

## Activation (Final Step)

1. Deploy the code to heroku by pushing the Git changes:

    ```sh
    git push heroku master
    ```

1. Set up a scheduled job (via the [Heroku Scheduler]() add-on) to run the Amazon Fresh check every 10 minutes:

    1. Go to https://dashboard.heroku.com/apps/REPLACE-WITH-APP-NAME/scheduler.

    2. Add a job to execute `node index.js` every 10 minutes.

1. That's it! The checker should run in T minus 10 minutes from now.

## Maintenance

- **Use of `DISABLED` Heroku app config variable.**

    To avoid spamming you with texts when a delivery window opens up, the application sets the `DISABLED` variable to `1` when it finds one. That way, the next time the app runs, it will not perform the check at all.

    On the flipside, if delivery windows are unavailable again and you wish to be notified of the _next_ availability, clear the `DISABLED` variable from the application configuration.