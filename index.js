const dotenv = require('dotenv');
const { JSDOM } = require('jsdom');
const fetch = require('node-fetch');

async function main() {
    dotenv.config();
    const {
        DISABLED,
        AMAZON_URL,
        AMAZON_HEADERS,
        TWILIO_URL,
        TWILIO_HEADERS,
        TWILIO_PAYLOAD,
        HEROKU_API_TOKEN,
        HEROKU_APP_ID,
    } = process.env;
    if (DISABLED) {
        return;
    }
    const response = await fetch(AMAZON_URL,{
        headers: JSON.parse(AMAZON_HEADERS),
    });
    const deliveryAvailable = !JSDOM.fragment(await response.text())
        .textContent.replace(/\s+/g, ' ')
        .match(/delivery temporarily sold out/i);
    if (deliveryAvailable) {
        await fetch(TWILIO_URL, {
            method: 'post',
            headers: JSON.parse(TWILIO_HEADERS),
            body: new URLSearchParams(JSON.parse(TWILIO_PAYLOAD)),
        });
        console.info('SMS notification sent. Disabling further executions.');
        await fetch(
            `https://api.heroku.com/apps/${HEROKU_APP_ID}/config-vars`,
            {
                method: 'patch',
                headers: {
                    Authorization: `Bearer ${HEROKU_API_TOKEN}`,
                    Accept: 'Accept: application/vnd.heroku+json; version=3',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ DISABLED: 1 }),
            },
        );
    } else {
        console.log('No delivery windows found.');
    }
}

main().catch(error => {
    console.error(error);
    process.exit(1);
});